/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
'use strict';

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.font_names='Open Sans;'+ config.font_names;
        //config.font_defaultLabel = 'Open Sans';

        config.contentsCss=[ 'https://fonts.googleapis.com/css?family=Open+Sans:Light' ]; 
     

};
